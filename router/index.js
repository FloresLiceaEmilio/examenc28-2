import express from 'express';
import json from 'body-parser';
export const router = express.Router();


//declarar primer ruta por omision


export default {router}


router.get('/', (req,res)=>{
    //parametros
    const params = {
        NDocente:req.query.NDocente,
        Nombre:req.query.Nombre,
        Domicilio:req.query.Domicilio,
        Nivel:req.query.Nivel,
        PHB:req.query.PHB,
        Horas:req.query.Horas,
        Hijos:req.query.Hijos
    }
    res.render('index',params);
})

router.post('/', (req,res)=>{
    //parametros
    const params = {
        NDocente:req.body.NDocente,
        Nombre:req.body.Nombre,
        Domicilio:req.body.Domicilio,
        Nivel:req.body.Nivel,
        PHB:req.body.PHB,
        Horas:req.body.Horas,
        Hijos:req.body.Hijos
    }
    res.render('index',params);
})